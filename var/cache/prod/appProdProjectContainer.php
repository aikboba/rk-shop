<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container2f0vjb5\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container2f0vjb5/appProdProjectContainer.php') {
    touch(__DIR__.'/Container2f0vjb5.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\Container2f0vjb5\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \Container2f0vjb5\appProdProjectContainer([
    'container.build_hash' => '2f0vjb5',
    'container.build_id' => '8ff360b6',
    'container.build_time' => 1569487832,
], __DIR__.\DIRECTORY_SEPARATOR.'Container2f0vjb5');
